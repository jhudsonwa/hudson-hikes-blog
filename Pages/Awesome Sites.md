Looking for other great hiking/backpacking websites and blogs? Check these out!

**Barefoot Jake**  
<a title="http://www.barefootjake.com" href="http://www.barefootjake.com" target="_blank">http://www.barefootjake.com</a></strong>

<a href="http://barefootjake.com" target="_blank">
![barefootjake.com](https://farm9.staticflickr.com/8712/16780943960_4666b6558f_z.jpg)
</a>

----------

**Exotic Hikes**  
*The Most Passionate Hiking and Guides, serving the Olympic National Park and Olympic Peninsula*

<a title="http://exotichikes.com/" href="http://exotichikes.com/" target="_blank">http://exotichikes.com</a>

<a href="http://exotichikes.com" target="_blank">
![exotichikes.com](https://farm9.staticflickr.com/8707/16942490626_e86ff055ac_o.png)
</a>

----------

**Blues Boots**  
*Another Pacific Northwest Hiker who hikes in the Pacific Northwest as well as around the United States. Follow Andrea's adventures on the trail!*

<a title="http://bluesboots.com/" href="http://bluesboots.com/" target="_blank">http://bluesboots.com/</a>

<a href="http://bluesboots.com" target="_blank">
![Blue's Boots](https://farm8.staticflickr.com/7645/16780786638_20dae4870f_o.png)
</a>

----------

**Solo Northwest Hiker**  
*Another Pacific Northwest Hiker who hikes a ton in the Olympic Mountains. She is also preparing for a AT thru hike next year. *

<a title="http://solonwhiker.com" href="http://solonwhiker.com" target="_blank">http://solonwhiker.com</a>

<a href="http://solonwhiker.com" target="_blank">
![Solo NW Hiker](https://farm8.staticflickr.com/7601/16942616036_8a8a23b58f_o.png)
</a>


----------

**NWHikers Forum**
*Great place to get trip reports and talk with other Pacific Northwest Hikers*

<a title="nwhikers.net" href="nwhikers.net" target="_blank">http://nwhikers.com</a>

<a href="http://nwhikers.net" target="_blank">
![NWHikers](https://farm9.staticflickr.com/8689/16761112427_0a198a0208_o.png)
</a>
