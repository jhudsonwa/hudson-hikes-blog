**Route:** McLane Creek Nature Trail  
**Distance:** Around A 1 Mile  
**Photos:** <a title="Flickr Set" href="https://www.flickr.com/photos/the_hudsons/sets/72157630181758250/" target="_blank">Flickr Set</a>

![Map](https://farm8.staticflickr.com/7608/16383392694_c896882578_z.jpg)  
*Route*

My family and I went out for a short nature walk around the McLane Creek Nature Trail. We explored the main trail around the beaver pond and then checked out some of the shorter side trails.

The McLane Creek Nature Trail, which is located just outside Olympia, Washington circles around a beaver pond and along the McLane Creek. It is a very family friendly trail and offers a boardwalk over the most of the trail so you can keep out of the mud. Lots of birds call the pond home and if you are lucky you can see turtles and spawning salmon if you go in the fall.

![Beaver Pond](https://farm9.staticflickr.com/8709/16819630339_afbde645fa_z.jpg)  
*Beaver Pond*

![Trail](https://farm9.staticflickr.com/8752/17005832305_9f01118f25_z.jpg)  
*Trail*

![McLane Creek](https://farm9.staticflickr.com/8752/16798438897_57a1f4b900_z.jpg)  
*McLane Creek*

![Beaver Pond](https://farm9.staticflickr.com/8735/16818330220_bd27fa5516_z.jpg)  
*Beaver Pond*