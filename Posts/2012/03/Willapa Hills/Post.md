**Route:** Parents House to Willapa Hills  
**Distance:** Around 8 Miles (Round Trip)  
**Days:** 2  
**Longest Day:** 4 Miles  

We got some new gear over the winter and wanted to try it out so we hiked the 4 miles from my parent's house to some property they own in the Willapa Hills. This was just a quick overnight trip.

![View From Camp](https://farm6.staticflickr.com/5552/14788547291_4db988e7ce_z.jpg)  
*View From Camp*

![Dusk](https://farm3.staticflickr.com/2920/14605167287_bdaf23c44e_z.jpg)  
*Dusk*

![Fire](https://farm3.staticflickr.com/2907/14791681945_1949450102_z.jpg)  
*Fire*

![Morning Fog](https://farm3.staticflickr.com/2902/14605051368_9e38feb59c_z.jpg)  
*Morning Fog*

![Camp](https://farm4.staticflickr.com/3848/14604976950_5c40b3d5e7_z.jpg)  
*Camp*
