**Route:** Graves Creek Trailhead To Enchanted Valley (Round Trip)  
**Distance:** Around 28 Miles  
**Days:** 2    
**Longest Day:** 14 Miles  
**Campsites:** Enchanted Valley  
**Photos:** <a title="Flickr Set" href="https://www.flickr.com/photos/the_hudsons/sets/72157630238524432/" target="_blank">Flickr Set</a>

<a href="https://farm8.staticflickr.com/7593/16967784355_aa249c062e_o.png" target="_blank">
![Map](https://farm8.staticflickr.com/7593/16967784355_f81267e173_z.jpg)  
*Click To View Full Size*
</a>

After our trip up to Mount Olympus on the <a title="Olympic National Park – Hoh River Trail" href="http://hudsonhikes.com/2011/08/22/olympic-national-park-hoh-river-trail/" target="_blank">Hoh River Trail</a> last year my Dad and I were ready to head back into the Olympic National Park. The Enchanted Valley is a huge valley nearly 14 miles up the &nbsp;East Fork of the Quinault River. The valley walls are covered in waterfalls and right in the middle of the valley is an old Chalet that was built in the early 1900s. The building now acts as a backcountry ranger station and emergency shelter. It's a pretty special place regardless of what time of year one&nbsp;hikes there.

![The Enchanted Valley](https://farm8.staticflickr.com/7637/16966520362_0a043f6799_z.jpg)  
*The Enchanted Valley*

<!--more-->

When I was 6 years old my family made the trip into the Enchanted Valley. I remember it rained a lot over the two days we were on the trail and in my memories the chalet contained a huge room to escape the rain and my brother and me were disappointed&nbsp;when it didn't have a McDonald's (We were 6 and 9!) . I don't remember much else on that trip. I just knew it was a place I wanted to see again.

We drove up the night before and camped at the Graves Creek Campground and then woke up early the next morning and hit the trail headed for the Enchanted Valley.

![Graves Creek Campground](https://farm8.staticflickr.com/7649/16941908066_0b4056952e_z.jpg)  
*Graves Creek Campground*

![Graves Creek Campground](https://farm8.staticflickr.com/7655/16780172078_e7a2ca2676_z.jpg)  
*Me On The Graves Creek Bridge*

![Trailhead Sign](https://farm8.staticflickr.com/7624/16780392570_97c11bfe86_z.jpg)  
*Trailhead Sign*

We had perfect weather for the way to the valley. The weather was sunny and clear with temperatures in the high 60's.

![Sunny Day On The Trail](https://farm8.staticflickr.com/7606/16347785483_6484bccec8_z.jpg)  
*Sunny Day On The Trail*

We left the trailhead around 8. The trail gained some elevation right off the bat for the first couple of miles, then dropped down to Pony Bridge.

![Quinault River Near Pony Bridge](https://farm8.staticflickr.com/7622/16941934356_4bc9c83d9a_z.jpg)  
*Quinault River Near Pony Bridge*

![Pony Bridge](https://farm9.staticflickr.com/8706/16966947351_590c11e9a0_z.jpg)  
*Pony Bridge*

![Quinault River Under Pony Bridge](https://farm8.staticflickr.com/7284/16781688199_6a7a9b8635_z.jpg)  
*Quinault River Under Pony Bridge*

After Pony Bridge the trail was mostly flat the rest of the way, gaining 1000ft of elevation over the next 10+ miles as it winds next to the Quinault River.

![Quinault River](https://farm8.staticflickr.com/7632/16967892475_a2597f9c7f_z.jpg)  
*Quinault River*

![Quinault River](https://farm9.staticflickr.com/8724/16966561022_394259a8d4_z.jpg)  
*Quinault River*

![Fire Creek](https://farm8.staticflickr.com/7284/16781688199_6a7a9b8635_z.jpg)  
*Fire Creek*

![Fire Creek](https://farm8.staticflickr.com/7630/16347806633_b7b37e4bcc_z.jpg)  
*Tall Trees, Blue Sky*


On the way to the the Enchanted Valley we saw lots of wildlife. Along the way we spotted&nbsp;2 small herd of elk (maybe 10 head in each, though I'm sure there were more we could not see), as well as 4 black bears. All the bears but the first one were more interested in finding berries and insects than us. I think we started the first bear because she was a little grumpy with us and we quickly moved on.

![Bear #1 - Grumpy Bear](https://farm8.staticflickr.com/7617/16941952916_162424f054_z.jpg)  
*Bear #1 - Grumpy Bear*

![Bear #2](https://farm9.staticflickr.com/8684/16781706299_5fb6cf7dc5_z.jpg)  
*Bear #2*

![Bear #3](https://farm8.staticflickr.com/7607/16967914035_81eb129c90_z.jpg)  
*Bear #3*

![Bear #4](https://farm9.staticflickr.com/8738/16966975611_1460d486b6_z.jpg)  
*Bear #4*

There are a ton of creek crossings on the trail but the only 'difficult' one was Pyrites Creek. The main log bridge is out, but their is a small log bridge that has been notched for safe crossing. It was narrow, but we made it across without getting wet or falling in.

![Pyrites Creek](https://farm8.staticflickr.com/7615/16966978211_5fb3d2a742_z.jpg)  
*Pyrites Creek*

After a long 13.5+ mile day we finally arrived in the Enchanted Valley. And it was as special of a place as I had remembered it. Waterfalls cascaded down the surrounding cliffs and two black bears spent the evening around our camp grazing for food (We were not on the menu).

![Bridge Over The Quinault River](https://farm8.staticflickr.com/7587/16760535527_ac5c2a154b_z.jpg)  
*Bridge Over The Quinault River*

![Quinault River](https://farm8.staticflickr.com/7623/16760539377_ba3404a7d8_z.jpg)  
*Quinault River*

![Waterfalls](https://farm9.staticflickr.com/8755/16347835363_87be98e2e6_z.jpg)  
*Waterfalls*

![Chalet In The Valley](https://farm8.staticflickr.com/7637/16966520362_0a043f6799_z.jpg)  
*Chalet In The Valley*

![Enchanted Valley Chalet](https://farm9.staticflickr.com/8751/16780460150_bfb87d3feb_z.jpg)  
*Enchanted Valley Chalet*

![Camp View](https://farm8.staticflickr.com/7282/16345574454_823043472d_z.jpg)  
*Camp View*

![Fire](https://farm9.staticflickr.com/8743/16781738349_86b416675f_z.jpg)  
*Fire*

![Enchanted Valley Bear #1](https://farm8.staticflickr.com/7594/16760556797_864863f780_z.jpg)  
*Enchanted Valley Bear #1*

![Enchanted Valley Bear #2](https://farm8.staticflickr.com/7634/16780253978_0dcaf6f2bb_z.jpg)  
*Enchanted Valley Bear #2*

After dinner we headed up a little further up the trail and ran into snow fairly quickly on the way towards Anderson Pass.

![Snow On The Trail And Mt. Anderson](https://farm9.staticflickr.com/8729/16967950005_8fd3bed404_z.jpg)  
*Snow On The Trail And Mt. Anderson*

We headed back to our camp and sat around the fire watching the bears. Once it started getting dark we crawled into our tent and went to sleep. After a 13.5+ day with a full pack we fell&nbsp;asleep fast.

The next morning it was time to head back out. We only had two days for this trip so we packed everything up and said goodbye to the valley.

![Heading Out](https://farm9.staticflickr.com/8719/16760564427_411c8e15cb_z.jpg)  
*Heading Out*

The rain stayed away until we crossed the bridge on the way out of the valley and then it then rained for the next 13 miles. At least we had one dry day in the rainforest. :)

![Wildflower](https://farm9.staticflickr.com/8708/16760566027_8343a01127_z.jpg)  
*Wildflower*

![Back At The Trailhead](https://farm8.staticflickr.com/7644/16967012691_32c1eff93a_z.jpg)  
*Back At The Trailhead*

It was an awesome trip and a lot of fun to get back into a place we had not been for over 25 years. I think we will have to make this a trip we do every year. It's that special of a place.

![Elevation In](https://farm9.staticflickr.com/8694/16760814437_d3624987ef_z.jpg)  
*Elevation In*

![Elevation Out](https://farm8.staticflickr.com/7610/16780509918_c7c46bdcf9_z.jpg)  
*Elevation Out*