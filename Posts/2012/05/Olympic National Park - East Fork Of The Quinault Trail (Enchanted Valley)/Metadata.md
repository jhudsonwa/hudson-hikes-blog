**Categories:**

- Backpacking
- Hiking
- Trip Reports

**Tags:**

- Backpacking
- Chalet
- Creeks
- East Fork Of The Quinault Trail
- Enchanted Valley
- Hiking
- Olympic Mountains
- Olympic National Park
- Quinault
- Quinault Rainforest
- Quinault River
- Wilderness
- River
- Bear