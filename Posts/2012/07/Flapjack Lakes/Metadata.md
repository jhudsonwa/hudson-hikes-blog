**Categories:**

- Backpacking
- Hiking
- Trip Reports

**Tags:**

- Backpacking
- Flapjack Lakes
- Flapjack Lakes Trail
- Gladys Divide
- Hiking
- North Fork Skokomish River Trail
- Olympic Mountains
- Olympic National Park
- Staircase, Wilderness