**Route:** Staircase to Flapjack Lakes and Gladys Divide
**Distance:** Around 20 Miles   
**Days:** 2    
**Longest Day:** 14 Miles  
**Campsites:** Flapjack Lakes  
**Photos:** <a title="Flickr Set" href="http://www.flickr.com/photos/the_hudsons/sets/72157630559874010/" target="_blank">Flickr Set</a>

<a href="https://farm8.staticflickr.com/7602/16348613483_c83fd7b7bb_o.png" target="_blank">
![Map](https://farm8.staticflickr.com/7602/16348613483_d47f919a44_z.jpg)  
*Click To View Full Size*
</a>

Dad and me took advantage of the amazing weather this last weekend to do a overnight backpacking trip up to Flapjack lakes in the Olympic National Park.

![Ready To Head Out](https://farm8.staticflickr.com/7632/16781304900_e977fa750c_z.jpg)  

<!--more-->

The first 3.5 miles up the North Fork Skokomish River Trail are nice and flat.

![North Fork Skokomish River](https://farm9.staticflickr.com/8736/16968784705_cae04a5eb2_z.jpg)
*North Fork Skokomish River*

![Creek Crossing](https://farm8.staticflickr.com/7626/16781310480_4c045cf497_z.jpg)
*Creek Crossing*

![Beaver Fire Sign](https://farm9.staticflickr.com/8718/16781097148_caf56da6e0_z.jpg)
*Beaver Fire Sign*

![Steve On The Trail](https://farm8.staticflickr.com/7607/16781098798_5b1a1b661d_z.jpg)
*Steve On The Trail*

![Flapjack Lakes Trailhead Sign](https://farm8.staticflickr.com/7647/16942840916_6cc6431e7c_z.jpg)
*Flapjack Lakes Trailhead Sign*

At 3.5 miles you take a right and hike a 4 mile climb (with the steepest part coming the last 1/2 mile) to the Flapjack Lakes (3900').

![Flapjack Lakes Trail](https://farm8.staticflickr.com/7635/16968793675_c0fecb271c_z.jpg)
*Flapjack Lakes Trail*

![Madeline Creek Bridge](https://farm8.staticflickr.com/7634/16781317110_35e399acdf_z.jpg)
*Madeline Creek Bridge*

![Waterfall](https://farm8.staticflickr.com/7629/16967855491_064d327a69_z.jpg)
*Waterfall*

![Flapjack Lakes Sign (.5 Mile To Go)](https://farm8.staticflickr.com/7285/16942846646_8c2411a809_z.jpg)
*Flapjack Lakes Sign (.5 Mile To Go)*

![Flapjack Lake](https://farm9.staticflickr.com/8688/16761411407_cfa7546726_z.jpg)
*Flapjack Lake*

The climb was pretty intense, especially with our backpacks, but the lakes were beautiful, and there were only about 5 other hikers staying at the lakes.

After a nap in camp we ate dinner and sat on the shore of the lake.

![Flapjack Lake](https://farm9.staticflickr.com/8751/16968802585_550d53e5d1_z.jpg)
*Flapjack Lake*

In the morning we did the hike up to Gladys Divide. We left Flapjack Lakes at 6 AM to see if we could catch the sun coming over the mountains. We hit snow about 1/4 mile past the lakes, and route finding was required to get to the top of the divide. There was still a ton of snow at the top (10+ feet) but it was very walk-able (we did the hike in regular hiking boots) and the views were simply amazing!

![Eagle At Flapjack Lakes](https://farm9.staticflickr.com/8754/16781111948_4a1834ea4f_z.jpg)
*Eagle At Flapjack Lakes*

![Glady's Divide Trail](https://farm9.staticflickr.com/8707/16782605569_ecf1f2ea6f_z.jpg)
*Glady's Divide Trail*

![Snow Covered Tarn](https://farm8.staticflickr.com/7605/16761418057_4a621c397c_z.jpg)
*Snow Covered Tarn*

![Sun Rising Over Glady's Divide](https://farm8.staticflickr.com/7631/16781115718_afdd91617e_z.jpg)
*Sun Rising Over Glady's Divide*

![Glady's Divide](https://farm8.staticflickr.com/7634/16348713473_f87ee591ce_z.jpg)
*Glady's Divide*

![View From Glady's Divide](https://farm9.staticflickr.com/8753/16967871581_8b4ba6cfac_z.jpg)
*View From Glady's Divide*

![5000ft!](https://farm9.staticflickr.com/8754/16782614399_a3dd2774aa_z.jpg)
*5000ft!*

After enjoying the views on Glady's Divide we hiked the full 9.5+ miles out back to the trailhead. It was another great weekend in the Olympic National Park.

![Back At The Trailhead](https://farm8.staticflickr.com/7288/16781274378_0da6a06dc4_z.jpg)
*Back At The Trailhead*