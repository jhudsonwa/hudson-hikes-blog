**Categories:**

- Backpacking
- Hiking
- Trip Reports

**Tags:**

- Backpacking
- Deer Lake
- Lake
- High Divide Trail
- Hoh Lake Trail
- Sol Duc River Trail
- Sol Duc River
- Sol Duc Falls
- Waterfalls
- Hiking
- Olympic Mountains
- Olympic National Park
- River
- Wilderness
- Seven Lakes Basin
- Lunch Lake
- Bogachiel Peak
- Heart Lake
- Elk
- Bear