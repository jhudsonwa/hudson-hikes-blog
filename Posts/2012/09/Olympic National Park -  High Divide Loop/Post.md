**Route:** High Divide Loop - Side Trip To Hoh Lake  
**Distance:** 22 Miles   
**Days:** 2   
**Longest Day:** Around 12 Miles  
**Campsites:** Hoh Lake   
**Photos:** <a title="Flickr Set" href="http://www.flickr.com/photos/the_hudsons/sets/72157632243557573/" target="_blank">Flickr Set</a>

<a href="https://farm9.staticflickr.com/8728/16972978731_3203f7e974_o.png" target="_blank">
![Map](https://farm9.staticflickr.com/8728/16972978731_09161556b2_z.jpg)  
*Click To View Full Size*
</a>

Dad and I wanted one last hike to cap off a great year of hiking. The weather was amazing so we packed up the car and headed north around the Olympic Peninsula to hike the High Divide Trail. 

When I was little my family hiked up to Deer Lake and then day hiked up to the High Divide but I had never done the loop before. It was time to change that.

![At The Trailhead](https://farm8.staticflickr.com/7626/16948503936_40b61b413f_z.jpg)  
*At The Trailhead*

We left the trailhead around 9 in the morning and made good time to Sol Duc Falls.

![At The Trailhead](https://farm9.staticflickr.com/8745/16786796878_83f55bfc77_z.jpg)  
*Sol Duc Falls*

From the falls we headed up the Deer Lake Trail towards Deer Lake. 

![Deer Lake Trail](https://farm8.staticflickr.com/7608/16948568036_11e4dd9336_z.jpg)  
*Deer Lake Trail*

The 3 miles from Sol Duc Falls to Deer Lake are fairly step and rocky. However, about half way up to Deer Lake there is a tree with a butt. That sight helped us forget that we were climbing as we joked about it and before long we had made it to Deer Lake.

![Deer Lake Trail](https://farm8.staticflickr.com/7282/16767131307_cf1bacff32_z.jpg)  
*Deer Lake Trail*

![Butt Tree](https://farm9.staticflickr.com/8690/16973565621_976f33c04b_z.jpg)  
*Butt Tree*

![Deer Lake](https://farm9.staticflickr.com/8750/16948540746_8f9f042862_z.jpg)  
*Deer Lake*

After eating lunch at Deer lake we continued the climb up to the High Divide. The trail is a fairly constant climb from the lake to the High Divide. But the Fall colors where vibrant so it was easy to keep our minds off the fact that we had climbed for nearly 8 miles.

![Heading Out From Deer Lake](https://farm9.staticflickr.com/8688/16959736886_c10e592710_z.jpg)  
*Heading Out From Deer Lake*

![Small Tarn](https://farm8.staticflickr.com/7640/16778300877_a824098da6_z.jpg)  
*Small Tarn*

![High Divide Trail](https://farm8.staticflickr.com/7644/16797959708_9756239729_z.jpg)  
*High Divide Trail And Fall Colors*

![Rocky Trail](https://farm8.staticflickr.com/7648/16985676435_6f3e85f58f_z.jpg)  
*Rocky Trail*

After a few miles of climbing we got our first good look at the 7 Lakes Basin and Lunch Lake. I remember when I was a small kid both my brother and I brought our fishing poles up to Lunch Lake. The water was so clear and blue. Even though we did not catch any fish and I was only 7 years old it is still one of my clearest memories. It was really neat to see it again all these years later.

![7 Lakes Basin Sign](https://farm8.staticflickr.com/7613/16984786521_018b47d65f_z.jpg)  
*7 Lakes Basin Sign*

![Lunch Lake](https://farm8.staticflickr.com/7596/16798234730_5a9bebe518_z.jpg)  
*Lunch Lake*

After enjoying the view of Lunch Lake we continued our climb up towards the High Divide. Just before the top is a fork and we took the Hoh Lake Trail towards Hoh Lake to camp for the night. 

![Hoh Lake Trail](https://farm9.staticflickr.com/8729/16984342102_72eca0dea4_z.jpg)  
*Hoh Lake Trail*

![Olympic Marmot](https://farm8.staticflickr.com/7586/16984343912_fb3f079e39_z.jpg)  
*Olympic Marmot*

Hoh Lake is 1.2 miles from the Junction. We hiked around the edge of a ridge and then dropped down to the lake. Coming into the lake we saw a black bear on the other side of the ridge. We never got close enough to it to get good pictures, but we did take the opportunity to walk around the lake.

![Hoh Lake](https://farm8.staticflickr.com/7621/16797972268_fe72f928a6_z.jpg)  
*Hoh Lake*

![Hoh Lake](https://farm9.staticflickr.com/8731/16798248140_90933fdce5_z.jpg)  
*Hoh Lake*

Hoh Lake also has a stunning view of Mount Olympus and as the sun started to set we were greeted with an alpine glow on the mountain.

![Mount Olympus](https://farm8.staticflickr.com/7643/16984800301_6dd78b05cb_z.jpg)  
*Mount Olympus*

After a long day of climbing we climbed into our tent and slept well.

The next morning we woke up and headed out back towards the High Divide. We had to hike the rest of the High Divide Loop today which would result in around 12 miles of Hiking. We took a few minutes once we left the lake to enjoy the view of the Hoh River and Mount Olympus then hiked backed up to the High Divide Trail seeing a bull elk along the way.

![Ready To Leave Hoh Lake](https://farm9.staticflickr.com/8743/16799514439_0821d06641_z.jpg)  
*Ready To Leave Hoh Lake*

![Looking Towards The Hoh River And Mount Olympus](https://farm9.staticflickr.com/8738/16363337844_da3f431570_z.jpg)  
*Looking Towards The Hoh River And Mount Olympus*

![Bull Elk](https://farm8.staticflickr.com/7593/16799674809_094b4f3589_z.jpg)  
*Bull Elk*

![High Divide Trail Junction Sign](https://farm9.staticflickr.com/8733/16984962271_973cc4b963_z.jpg)  
*High Divide Trail Junction Sign*

Once back on the High Divide trail we took a short detour to Bogachiel Peak and then worked our way across the High Divide towards Heart Lake. The entire way was full of Fall colors and a constant view of Mount Olympus and the Blue Glacier.

![7 Lakes Basin From Bogachiel Peak](https://farm9.staticflickr.com/8705/16984358852_00584c8a4a_z.jpg)  
*7 Lakes Basin From Bogachiel Peak*

![Me On The High Divide Trail](https://farm8.staticflickr.com/7584/16797987808_1d46a46467_z.jpg)  
*Me On The High Divide Trail*

![Fall Colors](https://farm8.staticflickr.com/7590/16959768816_53b5758cb1_z.jpg)  
*Fall Colors*

![Mount Olympus And Blue Glacier](https://farm8.staticflickr.com/7645/16799523929_66f1784417_z.jpg)  
*Mount Olympus And Blue Glacier*

![Mount Olympus](https://farm9.staticflickr.com/8740/16365624023_7418c7516b_z.jpg)  
*Mount Olympus*

![Heart Lake](https://farm8.staticflickr.com/7627/16365628153_47d4ed29ed_z.jpg)  
*Heart Lake*

We ate a quick lunch at Heart Lake and then hiked down to the Sol Duc River Trail and stayed on it all the way back to the trailhead.

![Hiking Downhill Finally!](https://farm8.staticflickr.com/7636/16797998518_d085bcc61a_z.jpg)  
*Hiking Downhill Finally!*

![Back At The Trailhead](https://farm8.staticflickr.com/7645/16778343047_0f88b219b8_z.jpg)  
*Back At The Trailhead!*

This trip should be on anyone's list who wants to spend some time in the Olympics. Especially in the fall.
