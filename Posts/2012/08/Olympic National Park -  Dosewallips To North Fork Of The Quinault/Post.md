**Route:** Dosewallips Washout To North Fork Of The Quinault Trailhead
**Distance:** Around 60 Miles   
**Days:** 6    
**Longest Day:** 17.1 Miles  
**Campsites:** Forest Service Road, Deception Creek, Dose Meadows, Hayes River Ranger Station, Low Divide  
**Photos:** <a title="Flickr Set" href="http://www.flickr.com/photos/the_hudsons/sets/72157631337348478/" target="_blank">Flickr Set</a>

<a href="https://farm9.staticflickr.com/8711/16968121252_a14d141762_o.png" target="_blank">
![Map](https://farm9.staticflickr.com/8711/16968121252_fdd834abf4_z.jpg)  
*Click To View Full Size*
</a>

![Goofing Off On Hayden Pass](https://farm9.staticflickr.com/8686/16968195512_ef7b5b699c_z.jpg)   

Days after my dad and I completed the 36 mile hike up to <a href="http://hudsonhikes.com/2011/08/22/olympic-national-park-hoh-river-trail/">Mount Olympus</a> in 2011 we started looking into where we would take a multi-day hike this year. After much discussion we finally settled on a thru hike of the park from the Dosewallips Road washout to the North Fork of the Quinault trail head with a side trip to Lost Pass.

<!--more-->

This roughly 60 mile route would take us on the following trails:

***Dosewallips River Trail***  
![Dosewallips River Trail](https://farm8.staticflickr.com/7654/16783439789_66f2e2828f_z.jpg)

***Lost Pass Primitive Trail***  
![Lost Pass Primitive Trail](https://farm8.staticflickr.com/7613/16783343810_b8fc6c672e_z.jpg)

***Hayden Pass Trail***  
![Hayden Pass Trail](https://farm9.staticflickr.com/8737/16763428037_ebb5d4d697_z.jpg)

***Elwha River Trail***  
![Elwha River Trail](https://farm8.staticflickr.com/7611/16763432227_0f2b3396c8_z.jpg)

***North Fork Of The Quinault Trail***  
![North Fork Of The Quinault Trail](https://farm8.staticflickr.com/7590/16350734293_f6f8c6a4ba_z.jpg)

This route would let us experience most of the terrain types of the Olympic National Park as well as experience several river fords (Something we were a bit nervous about, but turned out to not be a big deal). Several years ago there was a major washout 5.5 miles from the Dosewallips trail head. This does add road miles to this route, however the road is mostly grown over with vegetation now and is fairly easy walking.

**Day 1: Aug 4th, 2012** - Washout To Forest Service Road

We left on my 31st birthday and because we spent the day with family we got a very late start.

![Late Start](https://farm8.staticflickr.com/7633/16784679189_7599167f1a_z.jpg)  
*Late Start*

We were hoping we could get the 5.5 miles of road hike out-of-the-way before setting up camp. However, since we didn't get to the washout until after dark, we hiked about a 1/4 mile and camped on an abandoned forest service road.

![First Campsite](https://farm9.staticflickr.com/8743/16350786053_afdde4c198_z.jpg)  
*First Campsite*

Even though we didn't hike very far in the dark I did twist my ankle on a root before we setup camp (Be very careful hiking at night, even with headlights). Luckily, with my boots I could still walk fine but for the next 3 days I did pay for it every time we stopped for the night with a fair amount of pain. Nothing was going to stop us on this hike though. We waited almost a whole year to go!

**Day 2: Aug 5th, 2012** - Forest Service Road To Deception Creek

We got a very early start and hiked up the Dosewallips road.

![Getting Ready To Go](https://farm9.staticflickr.com/8713/16763590497_9e8461a724_z.jpg)  
*Getting Ready To Go*

![Entering The Park](https://farm8.staticflickr.com/7632/16783343868_1c44031237_z.jpg)  
*Entering The Park*

![Waterfalls](https://farm8.staticflickr.com/7624/16784790469_dfe8b7425f_z.jpg)  
*Waterfalls*

![Dosewallips Ranger Station](https://farm8.staticflickr.com/7632/16970098461_42a1b95f68_z.jpg)  
*Dosewallips Ranger Station*

After we got the 5.5 miles out-of-the-way and ate breakfast at the Dosewallips trail head.

![Dosewallips Trail Map](https://farm8.staticflickr.com/7643/16350938183_a1ba986666_z.jpg)  
*Dosewallips Trail Map*

After leaving the trail head we climbed steadily pretty much for the rest of the day. It was also very hot, with temperatures reaching the low 90's. Luckily there are a lot of creek crossings so water was never a problem.

![Backpack On Bridge](https://farm8.staticflickr.com/7589/16784794279_000fe9d4cf_z.jpg)  
*Backpack On Bridge*

![Constance Pass Junction Sign](https://farm8.staticflickr.com/7616/16350901003_6bdcc06d25_z.jpg)  
*Constance Pass Junction Sign*

![Creek Crossing](https://farm8.staticflickr.com/7655/16350910113_98b1837723_z.jpg)  
*Creek Crossing*

![Deception Creek Sign](https://farm8.staticflickr.com/7621/16970073721_b0c15cf3b3_z.jpg)  
*Deception Creek Sign*

![Deception Creek](https://farm8.staticflickr.com/7588/16784834619_c42db94427_z.jpg)  
*Deception Creek*

By the time we reached our next camp at Deception Creek we were very tired. We had hiked roughly 13 miles, most of them climbing in hot temperatures, and after eating dinner we built a fire and relaxed.

![Resting By The Fire At Deception Creek](https://farm9.staticflickr.com/8692/16970082431_604469a095_z.jpg)  
*Resting By The Fire At Deception Creek*

**Day 3: Aug 6th, 2012** - Deception Creek to Dose Meadows And Lost Pass

We got another early start. This was our shortest day on the trip. The plan was to go roughly 5 miles and set up camp at Dose Meadows. We were going to use this day as a rest day, knowing that the next couple days would be longer. Dad was doing pretty good, but I was dragging. 

![Me On The Trail To Dose Meadows](https://farm8.staticflickr.com/7584/16969798622_cc22d718bc_z.jpg)  
*Me On The Trail To Dose Meadows*

I'm not sure if it was dehydration or what, but this was the hardest 5 miles I've ever hiked. Every mile or so I would have to stop and rest, and I felt sick to my stomach. The views were stunning though.

![Gray Wolf Pass Sign](https://farm8.staticflickr.com/7654/16970219851_a639586ac4_z.jpg)  
*Gray Wolf Pass Sign*

![Looking Up Towards Gray Wolf Pass](https://farm8.staticflickr.com/7287/16783697310_345c96c11d_z.jpg)  
*Looking Up Towards Gray Wolf Pass*

![Lost Peak and Lost Pass](https://farm8.staticflickr.com/7633/16945222416_d7ab1988f2_z.jpg)  
*Lost Peak and Lost Pass*

![Tiger Lily](https://farm8.staticflickr.com/7284/16784975649_6ac36e3b33_z.jpg)  
*Tiger Lily*

And while it took us longer than expected, we did reach camp.

![Dose Meadows Sign](https://farm9.staticflickr.com/8692/16785007669_da356251fa_z.jpg)  
*Dose Meadows Sign*

![Olympic Marmot In Dose Meadows](https://farm9.staticflickr.com/8691/16945257296_81e98039bf_z.jpg)  
*Olympic Marmot In Dose Meadows*

After setting up camp at Dose Meadows we took a side trip up the 1 mile climb to Lost Pass. While it is only 1 mile, you gain roughly 1500 feet of elevation so it is a tough climb.

![Lost Pass Primitive Trail](https://farm8.staticflickr.com/7613/16783343810_b8fc6c672e_z.jpg)  
*Dad On The Last Pass Trail*

![Me On The Last Pass Trail](https://farm8.staticflickr.com/7633/16351161093_03f7d8b11f_z.jpg)  
*Me On The Last Pass Trail*

Without packs though it was a fun climb and I started feeling better the higher we climbed. Reaching the top made the entire day worth it.

![Dad And Me On Lost Pass](https://farm9.staticflickr.com/8750/16783829100_f68ee8928e_z.jpg)  
*Dad And Me On Lost Pass*

Simply put: Lost pass is stunning.

![Lost Pass](https://farm8.staticflickr.com/7282/16969932492_3dc00713b9_z.jpg)  
*Lost Pass*

There are views in all directions and all kinds of wildflowers covered the pass. I think I can say that this was my favorite part of the entire trip. Dad and I spent almost 2 hours on the pass just looking around and enjoying the views.

![Sentinel Peak, Mount Claywood And Cameron Pass From Lost Pass Panoramic](https://farm8.staticflickr.com/7629/16945317556_5916649dfb_z.jpg)  
*Sentinel Peak, Mount Claywood And Cameron Pass From Lost Pass Panoramic*

![Sentinel Peak, Mount Claywood And Cameron Pass From Lost Pass Panoramic](https://farm8.staticflickr.com/7628/16348901994_6633edef08_z.jpg)  
*Wellesley Peak, Thousand Acre Meadows And Sentinel Peak From Lost Pass Panoramic*

![Me On Lost Pass](https://farm8.staticflickr.com/7611/16783824390_4581824652_z.jpg)  
*Me On Lost Pass*

![Lost Pass Wildflowers](https://farm9.staticflickr.com/8743/16783575938_833a817091_z.jpg)  
*Lost Pass Wildflowers*

We finally made our way back down from Lost Pass and spent the rest of the day just resting and enjoying the Dose Meadows. The meadows are very peaceful and full of wildflowers. After dinner a buck deer decided it wanted to come visit us and walked right into camp. It was a great ending to the day.

![Dose Meadows](https://farm9.staticflickr.com/8717/16969954972_deeb9494d2_z.jpg)  
*Dose Meadows*

![Buck In Camp](https://farm8.staticflickr.com/7588/16763891267_78668a9dbe_z.jpg)  
*Buck In Camp*

**Day 4: Aug 7th, 2012** - Dose Meadows to Hayes River Ranger Station

Another early start and we were heading up towards Hayden Pass.

![Dose Meadows](https://farm9.staticflickr.com/8717/16785190539_29713d6d03_z.jpg)  
*Dose Meadows*

![Ready To Leave Dose Meadows](https://farm9.staticflickr.com/8714/16764000117_8bba0d6018_z.jpg)  
*Ready To Leave Dose Meadows*

I was feeling better and we made good time as we climbed the last few miles towards the pass.

![Dad On The Trail To Hayden Pass](https://farm8.staticflickr.com/7586/16971396565_58da9ee610_z.jpg)  
*Dad On The Trail To Hayden Pass*

![Hayden Pass](https://farm8.staticflickr.com/7606/16970451621_3d4887210b_z.jpg)  
*Hayden Pass*

We spent a few minutes at the base of Hayden Pass visiting with some other hikers and resting near the creeks that form the headwaters of the Dosewallips river before starting the last climb up the pass.

![One Of The Creeks That Becomes The Dosewallips River Coming Out Of The Rocks](https://farm9.staticflickr.com/8687/16783926500_f9062717f2_z.jpg)  
*One Of The Creeks That Becomes The Dosewallips River Coming Out Of The Rocks*

![Climbing Hayden Pass](https://farm9.staticflickr.com/8742/16351299963_0356106d00_z.jpg)  
*Climbing Hayden Pass*

Most of the snow was melted with only a couple of sections of the trail covered and about 45 minutes later we were standing on top of Hayden Pass. Once again the view was stunning. On one side of the pass we could see back down the Dosewallips River valley and on the other side we saw the Elwha River valley as well as Mt. Anderson, Mt. Olympus and the Bailey Range.

![Top Of Hayden Pass](https://farm9.staticflickr.com/8695/16945445616_225a9de60c_z.jpg)  
*Top Of Hayden Pass*

![View From Hayden Pass](https://farm8.staticflickr.com/7282/16783698648_8fb1c3cd55_z.jpg)  
*View From Hayden Pass*

![Mt. Olympus](https://farm8.staticflickr.com/7646/16351303283_8b70b552fb_z.jpg)  
*Mt. Olympus*

We then took off heading down from Hayden Pass towards our next camp in the Elwha River Valley. A few minutes after Hayden pass we came upon a bear that was sleeping and taking a bath in a small icy pond. He was not thrilled we woke up, glared at us and jumped over the edge into some trees.

![Ice Bear Was Sleeping On](https://farm8.staticflickr.com/7640/16783703398_4cbd6cb794_z.jpg)  
*The Ice Bear Was Sleeping On - If you Look Close You Can See Bear Entering Trees*

The rest of the day was mostly uneventful. The Hayden Pass trail is easy to go down on, it would be a difficult climb, but by 3 in the afternoon we had met up with the Elwha River trail and were soon at our next camp at the Hayes River Ranger Station.

![Elwha River Trail Sign](https://farm8.staticflickr.com/7610/16945451806_b98cb5d863_z.jpg)  
*Elwha River Trail Sign*

![Hayes River Ranger Station](https://farm9.staticflickr.com/8731/16783936810_de553d07b8_z.jpg)  
*Hayes River Ranger Station*

We spent the afternoon exploring around the ranger station and lounging in the nice chair that was on the front porch of the ranger station. Never underestimate how nice a chair can be after 30 miles of hiking. :)

![Finally Found A Chair In The Wilderness](https://farm9.staticflickr.com/8704/16971413195_ca195e548f_z.jpg)  
*Finally Found A Chair In The Wilderness!*

Day 4 ended up being an 11 mile day.

**Day 5: Aug 8th, 2012** Hayes River Ranger Station to Low Divide

![Ready To Head Out For Another Day](https://farm8.staticflickr.com/7628/16972960625_b18e8bf52e_z.jpg)  
*Ready To Head Out For Another Day*

![Bear Cans?](https://farm8.staticflickr.com/7587/16352873103_b285b8f2cf_z.jpg)  
*Bear Cans?!?! :)*

The plan for Day 5 was to hike from the Hayes River Ranger Station to Chicago Camp and have another shorter rest day. We set off and were making good time. The Elwha River Trail is a mostly flat trail, which was a nice change from all the climbing we had done to get there.

![Dad Crossing The Hayes River](https://farm8.staticflickr.com/7616/16786760099_b5ca246d26_z.jpg)  
*Dad Crossing The Hayes River*

![Elwha River Trail](https://farm8.staticflickr.com/7611/16763432227_0f2b3396c8_z.jpg)  
*Elwha River Trail*

We arrived at Chicago Camp around lunch time.

![Sign At Chicago Camp](https://farm9.staticflickr.com/8690/16947032666_af40a578b7_z.jpg)  
*Sign At Chicago Camp*

![Taking A Break At Chicago Camp](https://farm9.staticflickr.com/8702/16785293048_04e96ea5b9_z.jpg)  
*Taking A Break At Chicago Camp*

After a nice lunch by the river, we decided we were not ready to stop for the day and wanted to push up towards the Low Divide. The river ford at Chicago Camp was easy. There were a bunch of trees down so we could simply walk across. If we would have had to go through the water it would have been about knee-deep and was moving at a pretty good pace, so I'm glad there was an easier way across.

![Elwha River At Chicago Camp](https://farm8.staticflickr.com/7288/16947054216_7e2257a939_z.jpg)  
*Elwha River At Chicago Camp*

![ A Nice Path To Ford The River](https://farm8.staticflickr.com/7616/16765656747_5f08ec1cc0_z.jpg)  
*A Nice Path To Ford The River*

We knew that once we left Chicago camp we would have to climb the 3+ miles up to the Low Divide as there are no other places to camp. The climb was pretty steep, but we made it to the Low Divide in a couple of hours.

![Up, Up, Up](https://farm9.staticflickr.com/8743/16972092761_9825d5e7f4_z.jpg)  
*Up, Up, Up*

![Lake Mary](https://farm9.staticflickr.com/8706/16785542620_ba11c0fed4_z.jpg)  
*Lake Mary*

![Lake Margaret and Mount Seattle](https://farm9.staticflickr.com/8707/16971691722_dcb3d53c14_z.jpg)  
*Lake Margaret and Mount Seattle*

After resting at Lake Mary and Lake Margaret we made our way to the Low Divide and explored a bit before setting up camp. The Low Divide should have been awesome. The views were nice and the weather was mostly nice. However, the mosquitoes ate us alive. We ended up having to stay in the tent most of the evening to avoid them. Even with DEET they still attacked us through our clothes and I had over 50 bites. Nasty little buggers. I was very happy to get going the next morning. :)

![Low Divide](https://farm9.staticflickr.com/8742/16971687122_4e0d49d9eb_z.jpg)  
*Low Divide*

![Low Divide Camp](https://farm9.staticflickr.com/8709/16352923843_1d1bc56509_z.jpg)  
*Low Divide Camp*

![Clouds Rolling Into The Low Divide](https://farm9.staticflickr.com/8746/16350651804_858af82e62_z.jpg)  
*Clouds Rolling Into The Low Divide*

![Low Divide Camp](https://farm9.staticflickr.com/8699/16785402688_98d5072a89_z.jpg)  
*Low Divide Rangers Station*

Day 5 ended up being around a 12 mile Day.

**Day 6: Aug 9th, 2012** - Low Divide to North Fork Of The Quinault Trailhead

The plan for Day 6 was to go from the Low Divide to Elip creek which would put us about 6 miles from the North Fork of The Quinault trail head. We left the Low Divide without eating breakfast to avoid those nasty mosquitoes and made really good time over the 3.5 miles to the ford at 16 Mile.

![Ready To Head Out Again](https://farm9.staticflickr.com/8704/16971819072_3e8df2a574_z.jpg)  
*Ready To Head Out Again*

![Gate At The End Of The Low Divide](https://farm9.staticflickr.com/8749/16947208386_01d6cb7047_z.jpg)  
*Gate At The End Of The Low Divide*

![Waterfall And Wildflower Filled Meadow](https://farm9.staticflickr.com/8739/16765852027_6ae72c4441_z.jpg)  
*Waterfall And Wildflower Filled Meadow*

![Creek Running Through A Canyon](https://farm8.staticflickr.com/7590/16353132333_b728068f4c_z.jpg)  
*Creek Running Through A Canyon*

At the river we ate breakfast, mosquito free, and then forded the river. The ford at 16 Mile was the one ford we were most nervous about, but it ended up being a non issue. The water was between knee and lower calf deep and moving at a good pace, but with two poles (or sticks) we were able to make it across just fine.

![16 Mile Ford](https://farm9.staticflickr.com/8703/16787019799_33af3aa683_z.jpg)  
*16 Mile Ford*

![Made It Across!](https://farm8.staticflickr.com/7584/16786969999_cce14793a5_z.jpg)  
*Made It Across!*

After the 16 mile ford we kept a pretty steady pace along the North Fork Of The Quinault trail and made it to Elip Creek in the early afternoon.

![Taking A Break On The Trail](https://farm8.staticflickr.com/7632/16785517898_0e9cfde7d0_z.jpg)  
*Taking A Break On The Trail*

![Trappers Creek Trail](https://farm9.staticflickr.com/8733/16350846634_c587aa7b79_z.jpg)  
*Trappers Creek Shelter*

![Kimta Creek Bridge](https://farm8.staticflickr.com/7614/16973201705_ee0134025f_z.jpg)  
*Kimta Creek Bridge*

![Elip Creek Campsite](https://farm8.staticflickr.com/7287/16947240436_bde7efc1e6_z.jpg)  
*Elip Creek Campsite*

We ate lunch and decided that since we had already hiked 11 miles, we could probably walk 6 more and be out a day early. We were having a great time, but it would get us home to our family a day early as well, which after 6 days in the wilderness was nice. We lifted our backpacks up on our shoulders one last time and hiked out the last 6 miles.

![North Fork Of The Quinault River](https://farm8.staticflickr.com/7595/16973185835_6566278ae0_z.jpg)  
*North Fork Of The Quinault River*

![We Made It!](https://farm9.staticflickr.com/8743/16785713000_21a7cbc267_z.jpg)  
*We Made It!*

Day 6 ended up being around a 17 mile day.

**Final Thoughts:**

Overall this was an amazing trip and a great route. We met some great people, such as a couple of scout groups working on their 50 mile badges, and saw some stunning wilderness. I'm truly blessed I can spend this time and make lifetime memories with my dad and I can't wait to go on many more trips like this in the future.

![At The Parking Lot](https://farm8.staticflickr.com/7643/16947223646_88dd9c9403_z.jpg)  
*At The Parking Lot*