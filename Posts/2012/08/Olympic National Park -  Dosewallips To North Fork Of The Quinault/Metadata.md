**Categories:**

- Backpacking
- Hiking
- Trip Reports

**Tags:**

- Backpacking
- Creeks
- Dosewallips River Trail
- Elwha River Trail
- Hayden Pass
- Hayden Pass Trail
- Hiking, Lost Pass
- Lost Pass Primitive Trail
- North Fork Of The Quinault River
- North Fork Of The Quinault Trail
- Olympic Mountains
- Olympic National Park
- Quinault Rainforest
- River
- Wilderness
- Lost Pass
- Waterfalls