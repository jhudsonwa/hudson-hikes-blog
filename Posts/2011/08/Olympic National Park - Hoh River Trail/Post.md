**Route:** Trailhead To Blue Glacier (Round Trip)  
**Distance:** 36.2 Miles  
**Days:** 3  
**Longest Day:** 17 Miles  
**Campsites:** Martin Creek Stock Camp, Glacier Meadows  
**Photos:**  <a title="Flickr Set" href="https://www.flickr.com/photos/the_hudsons/sets/72157629724166452/" target="_blank">Flickr Set</a>  

<a href="https://farm6.staticflickr.com/5571/14810130163_65a7473d76_o.jpg" target="_blank">
![Click To View Full Size](https://farm6.staticflickr.com/5571/14810130163_f4f94a0227_c.jpg)
</a>

When I was little we used to go backpacking a lot. In fact, when I was 6 years old my family made the 26 mile round trip trek into the Enchanted Valley (Olympic National Park). For whatever, reason we stopped going. 24 years later, my dad and me got the itch to throw our packs back on and head into the wilderness once again.

Without any training and with gear from the 80's we decided that our first backpacking trip should probably be a short overnight trip. After some discussion we went right for a 3 Day, 36 mile hike to the base of Mount Olympus and back.

<!--more-->

We packed up our packs with everything and the kitchen sink (no really, we had a small foldable sink), pulled on our all cotton clothes and drove to the Hoh Rainforest Visitor Center, a 3-4 hour drive from where we live.

The plan, which we had spent all of 3 1/2 minutes coming up with, was to hike 10 miles on the first night (it was our first backpacking trip in 24 years after all), 8 miles the next day and then with all that 'on the job' experience and training (2 days of backpacking), we would hike the full 17+ miles out on the 3rd day. This plan lasted until we were at the Visitor Center talking to the ranger, who took one look at us and suggested we should change our plan a bit to maximize our chances of success (read as, not die) and hike shorter days and maybe, if we could, add a 4th day to the trip we should try that. Naturally, we ignored this ranger and instead of hiking 10 miles on the first day, we choose to hike 14.7 miles to make the 2nd day easier. The ranger wished us luck and then double checked that he had our family contact information on file just in case.

We left the Hoh Visitor center around 11:00 A.M. (My advice, try to leave earlier).

![Hoh Visitor Center Sign](https://farm3.staticflickr.com/2917/14787165411_e53209aea2_z.jpg)  
*Hoh Visitor Sign*

![All Ready To Go](https://farm3.staticflickr.com/2928/14603594660_cc2427866f_z.jpg)  
*All Ready To Go*

![Hoh River Trail Sign](https://farm4.staticflickr.com/3852/14603720708_cc1e9ecc46_z.jpg)  
*Hoh River Trail Sign*

The first 12.4 miles of this trail are pretty flat and we worked our way through the rainforest and up the Hoh River Valley. The scenery is amazing and this trail lives up to its name as one of the best in the Olympic National Park.

![Mossy Tree](https://farm4.staticflickr.com/3879/14791489715_6eee4624ae_z.jpg)  
*Mossy Tree*

![Looking Up The Hoh River](https://farm6.staticflickr.com/5588/14788357051_9ec25dd977_z.jpg)  
*Looking Up The Hoh River*

![First Waterfall On Trail](https://farm4.staticflickr.com/3864/14604980177_156f45f3fa_z.jpg)  
*First Waterfall On Trail*

![Happy Four Shelter](https://farm6.staticflickr.com/5551/14788357241_ca5254c676_z.jpg)  
*Happy Four Shelter*

![Standing By A Huge Fallen Tree](https://farm4.staticflickr.com/3855/14788358541_b40fd3eaa6_z.jpg)  
*Standing By A Huge Fallen Tree*

![Blue Water Of The Hoh River](https://farm3.staticflickr.com/2906/14811339873_5f116e79ec_z.jpg)  
*Blue Water Of The Hoh River*

At 12.4 miles we finally started to climb. We were both tired and ready to be at camp, but we still had 2 more miles to go and all of it was uphill. After a short climb we crossed a bridge that is 120+ feet above the Hoh River. It was around this time that we met a group of  hikers from the Seattle area. I should write that everyone should hike their own hike, and that backpacking is not a race or a competition, but of course we spent the next two days in a race with this group of hikers (we were probably the only ones racing). It was fun seeing them at different points over the next couple of days.

![Hoh River Canyon Bridge](https://farm6.staticflickr.com/5570/14604795060_1098415a84_z.jpg)  
*Hoh River Canyon Bridge*

![Hoh River 120+ Feet Below](https://farm4.staticflickr.com/3889/14768509016_43b9e27172_z.jpg)  
*Hoh River 120+ Feet Below*

After leaving the bridge all we did was climb up.. up.. up. Being our first hike in many years this climb quickly earned the nickname 'The Hill Of Death'. It was brutal and we climbed up switchbacks for 2 miles. I wonder what this climb would be like if we were in shape and better prepared.

After a long day of backpacking we finally arrived at Martin Creek.

![Martin Creek](https://farm3.staticflickr.com/2924/14604796470_0c20846c55_z.jpg)  
*Martin Creek*

We noticed a lot of hikers continued on to the camp at Elk Lake (only a bit further up the trail), but we really enjoyed the camp at Martin Creek. First, you can light a fire, and secondly there are fewer people who stay there. On the night we were there, we were the only campers.

![Martin Creek Camp](https://farm6.staticflickr.com/5567/14604991767_7aeac4e554_z.jpg)  
*Martin Creek Camp*

![Fire At Martin Creek Camp](https://farm4.staticflickr.com/3910/14791151972_9d592a28f3_z.jpg)  
*Fire At Martin Creek Camp*

The next morning we woke up and continued the hike up to the Glacier Meadows campground (Around 3 miles). Within 10 minutes we reached Elk Lake and continued on up the trail.

![Elk Lake](https://farm4.staticflickr.com/3857/14791156722_0bc944450f_z.jpg)  
*Elk Lake*

The trail from Martin Creek to Glacier Meadows was a pretty decent climb, but seemed easier than the climb up 'The Hill Of Death'. There are parts where the trail gets pretty narrow, and we had to be careful.

![Narrow Trail](https://farm4.staticflickr.com/3845/14789155344_81f06f6daa_z.jpg)  
*Narrow Trail*

About a half mile from the Glacier Meadows camp there had been a huge landslide and to continue we had to climb down a rope and ladder. It was a bit daunting but we took it slow and made it through this section of the trail. The landslide area was by far the most technical part of this trail. Luckily, most of the snow was melted on the landslide by the time we went through.

![Landslide And Ladder](https://farm3.staticflickr.com/2931/14604999167_0cb5e3bd26_z.jpg)  
*Landslide And Ladder*

![Ladder And Rope Up Landslide](https://farm4.staticflickr.com/3869/14604879548_ea6ff1d49d_z.jpg)  
*Ladder And Rope Up Landslide*

Once we had made it through the landslide we hiked the last half mile to the Glacier Meadows camp. 


After letting our legs rest a bit we setup camp and then hiked the last 1 mile to the real Glacier Meadows.

![View From Ridge Above Glacier Meadow](https://farm3.staticflickr.com/2936/14604806920_2ccd818e63_z.jpg)  
*View From Ridge Above Glacier Meadows*

![Glacier Meadows](https://farm6.staticflickr.com/5577/14811351663_da654627a3_z.jpg)  
*Glacier Meadows*

After a short rest, we climbed up the rest of the way to the edge of the Blue Glacier (18.1 miles from the trailhead). Not all the snow had melted for this last little climb, but we took it slow and made it to the top just fine without the use of any special equipment.

![Trail Up To Blue Glacier](https://farm6.staticflickr.com/5584/14791511005_1df6625eb6_z.jpg)  
*Trail Up To Blue Glacier*

The sky was clear and we got to see Mt. Olympus and the Blue Glacier in all their glory. I can honestly say that this is a view that I will forever treasure. Pictures can not do it justice and being on that ridge, even for an hour made the entire trip worth it.

![Mount Olympus And Blue Glacier](https://farm3.staticflickr.com/2929/14604851449_41981e8caf_z.jpg)  
*Mount Olympus And Blue Glacier*

<iframe src="//www.youtube.com/embed/2KMZOgWQ3Q8" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

We walked back to the Glacier Meadows campsites made dinner and then went to sleep.

![Glacier Meadows Camp](https://farm4.staticflickr.com/3854/14604882868_40c13b3967_z.jpg)  
*Glacier Meadows Camp*

We woke up the next morning by 6 a.m. and walked the full 17.3 miles out.

![All Ready To Go](https://farm4.staticflickr.com/3861/14768523716_76c6545855_z.jpg)  
*All Ready To Go*

![White Glacier](https://farm4.staticflickr.com/3892/14604884708_434ea6c0c6_z.jpg)  
*White Glacier*

![Mount Olympus Snow Dome](https://farm4.staticflickr.com/3910/14788381771_8a265abb57_z.jpg)  
*Mount Olympus Snow Dome*

It took 8 full hours to hike back to the trailhead, almost exactly, and I'm happy to report that we beat the hikers from Seattle by an hour! ;-)

If we do this hike again I would probably take the rangers advice and add an extra day or change the number of miles hiked each day to avoid 17 mile days.

The motto we came up with for this trail was: "Hard. But Worth It". It truly was, but I can't wait to get back out and explore more of the park.

![Back At The Trailhead](https://farm6.staticflickr.com/5574/14604856889_0c97e6ecc5_z.jpg)  
*Back At The Trailhead*